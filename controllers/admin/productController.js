const { products } = require("../../models");

async function home(req, res) {
    res.render("index")
}

// Menampilkan semua data
async function getProducts(req, res) {
    const productsData = await products.findAll()
    res.render("products/index", {
        productsData,
    });
};

// Merender halaman untuk menambahkan data
async function createProductPage(req, res) {
    res.render("products/create");
}

// untuk kalau menggunakan method post di ejs nya
async function createProduct(req, res) {
    const name = req.body.name
    await products.create({ 
        name: name 
    })
    res.redirect("/allProducts")
}

// Update product Page
async function updateProductPage(req, res) {
    const id = req.params.id
    const product = await products.findByPk(id)
    res.render("products/update", {
        product,
    });
}

// Delete product
async function deleteProduct(req, res) {
    const id = req.params.id
    await products.destroy({ 
        where: {id: id}
    })
    res.redirect("/allProducts")
}

// Update Product
async function updateProduct(req, res) {
    const id = req.params.id
    const name = req.body.name
    await products.update({ 
        name: name
    },{
        where: {id: id}
    })
    res.redirect("/allProducts")
}

module.exports = {
    home,
    getProducts,
    createProductPage,
    createProduct,
    updateProductPage,
    deleteProduct,
    updateProduct
}