const router = require("express").Router();

const productController = require('../controllers/productController')
const adminProductController = require('../controllers/admin/productController')

// API
router.get('/api/products', productController.getProducts)
router.post('/api/products', productController.createProduct)
router.get('/api/products/:id', productController.getProductById)
router.put('/api/products/:id', productController.updateProduct)
router.delete('/api/products/:id', productController.deleteProduct)
router.put("/update", productController.updateProduct);

// Halaman utama
router.get('/', adminProductController.home)

// dashboard admin
router.get('/allProducts', adminProductController.getProducts)
router.get('/products', adminProductController.createProductPage)
router.post('/products', adminProductController.createProduct)
router.get('/update/:id', adminProductController.updateProductPage)
router.get('/delete/:id', adminProductController.deleteProduct)
router.post('/update/:id', adminProductController.updateProduct)
module.exports = router